cid = get('cid') || 2104;
//u = 'https://api.ziwangame.com/pageapi/sempage/channel/page?cid=' + cid;
u = '//'+ cid;
domain = document.domain.split('.').slice(-2).join('.');
var type=2;

Zepto(function($){
	init_data();
});

function init_data(){
	var qrcode = new QRCode(document.getElementById('page_qr'),{width:180,height:180});
	qrcode.makeCode(window.location.href);
	
	$.ajax({type: 'GET',url: u + '&page_id=1&' + update_time(),async:false,success:function(data){
		var html = '';
		if(data != '' && data != null && data != undefined){
			var data = data.data;
			$.each(data,function (i,item){
				var no1 = '';
				if(i==1){no1='class="no1"'}
				html += '<a href="'+ item.app_url +'" data-url="'+ item.app_url +'" '+ no1 +'><img src="'+ item.icon +'" /><div class="name">'+ item.name +'</div><div class="scoring">'+ item.score +'分</div><div class="btn">下载</div></a>';
				if(i>=2){return false;};
			});
			//$('.rec_list').append(html);
			
			if(!ua('android') && !ua('linux') && !ua('iphone') && !ua('ipad')){
				pc_qrcode_rec();
			}
		}
	}});
	
	loadData(type);
	
	$('.nav li').on('click',function(){
		nav($(this).index());
	});
	
	$('nav a').on('click',function(){
		nav($(this).index(),2);
	});
	
	window.onscroll = function(){
		var osTop = document.documentElement.scrollTop || document.body.scrollTop;
		if (osTop >= 520) {
			$('nav').removeClass('hide');
		}else{
			$('nav').addClass('hide');
		}
	};
	
	$.ajax({type: 'GET',url:'http://go.ziwanyouxi.com/ip/getcity',success:function(data){
		if(data){
			if(data.length <=3){
				$('.city').text(data + '榜');
			}
		}
	}});
	
	$.ajax({type: 'GET',url: u + '&page_id=1&' + update_time(),success:function(data){
		if(data != '' && data != null && data != undefined){
			var copyright_text = data.copyright_text;
			$('.foot').text(copyright_text);
		}
	}});
	
	total();
}

function total(){
	var num = $.fn.cookie('total') || 335462;
	num = parseInt(num) + Math.floor(Math.random()*5);
	$.fn.cookie('total',(num),{expires:1,domain:domain});
	$('.total').text(num);
	setTimeout('total()', 1500);
}
	
var timer;
function tips(msg,color){
	clearTimeout(timer);
	var obj = $('.tips');
	obj.removeClass('green').removeClass('orange').addClass(color);
	obj.text(msg).fadeIn();
	timer = setTimeout(function(){obj.fadeOut()},4500);
}

function nav(i,t){
	if(t==2){document.documentElement.scrollTop = document.body.scrollTop = 480;};
		
	$('.nav li').removeClass('active');
	$('.nav li').eq(i).addClass('active');
	
	$('nav a').removeClass('active');
	$('nav a').eq(i).addClass('active');
	
	var type = $('.nav li').eq(i).attr('data-id');

	loadData(type);
}

function loadData(type){
	var type2 = type.toString().substr(0,1);
	var type = type.toString().replace(/a/,'');
	var url =  u + '&page_id='+ type +'&' + update_time();
	$.ajax({type: 'GET',url:url,async:false,success:function(data){
		var html = '';
		if(data != '' && data != null && data != undefined){
			var data    = data.data;
			var zan_num = 13000 - type * 300;
			var step    = 10000 / (data.length+1);
			
			if(type2 == 'a'){
				data.reverse();
			}
			
			$.each(data,function (i,item){
				var num,num_html,intro=item.intro,effects='';
				
				if(i < 3){
					num = 'r' + (i+1);
					num_html = '<i></i>';
				}else{
					num = '';
					num_html = '<span>'+ (i+1) +'</span>';
				}
				
				if(type == 7){
					var tags       = item.tags;
					var tags_html  = '';
					var tags_style = ['red','blue','green','orange'].sort(function(){return 0.5-Math.random()});
					
					tags.split(',').forEach(function(n,index){
						tags_html = tags_html + '<b class="'+ tags_style.slice(index,(index+1)) +'">' + n + '</b>';
					})
					intro = tags_html;
					
					if(item.order == '1'){
						$('.list_more').remove();
						$('.game_list').after('<a href="'+ item.app_url +'" data-url="'+ item.app_url +'" class="list_more more_qr">查看更多&gt;&gt;</a>');
					}
				}else{
					$('.list_more').remove();
					$('.game_list').after('<a href="javascript:nav(7);" class="list_more">想要更过瘾的？点击去玩BT服&gt;&gt;</a>');
				}
				
				$('.list_more').on('click',function(){
					document.body.scrollTop = document.documentElement.scrollTop = 400;
				});
				
				if(i == 0){
					effects = '<img class="effects" src="http://cdn-fe.ziwanyouxi.com/paihang/paihang-3/static/images/app_icon_effects.gif?1" />';
				}
				
				var zan_num_i = $.fn.cookie('zan_num_' + type + '_' + i);
				if(zan_num_i == null){
					zan_num = parseInt(zan_num - step * ((Math.random()*5+5)/10).toFixed(1));
					zan_num < 1 ? zan_num = 1 : zan_num;
					$.fn.cookie('zan_num_' + type + '_' + i,zan_num,{expires:1,domain:domain});
				}else{
					zan_num = zan_num_i;
				}
				
				html += '<li><a href="'+ item.app_url +'" data-url="'+ item.app_url +'"><div class="icon '+ num +'">'+ effects +'<img src="'+ item.icon +'" />'+ num_html +'</div><dl><dt>'+ item.name +'<dd>'+ intro +'<dd>玩家点赞<em>'+ zan_num +'</em><i class="btn_zan">点个赞</i></dl><div class="down_btn">下载游戏</div></a><div class="zan"></div>';
			});
			$('.game_list').html(html);
			
			if(!ua('android') && !ua('linux') && !ua('iphone') && !ua('ipad')){
				pc_qrcode_list();
			}
	
			$('.zan').on('click',function(){
				tips('为打造高质量榜单，请体验游戏1小时后，再来点赞支持！','orange');
			});
		}
	}})
} 
function pc_qrcode_rec(){
	$('.rec_list .btn').unbind('mouseover').on('mouseover', function(){
		var obj = $(this);
		var app_url = obj.parent().attr('data-url');
		obj.append('<div id="qrcode" class="qrcode_rec"><div>手机浏览器扫码</div></div>');
		$('#qrcode').css({top:'-2.08rem',left:'-.25rem',right:0});
		var qrcode = new QRCode(document.getElementById('qrcode'),{width:150,height:150});
		qrcode.makeCode(app_url); 
		//obj.parent().attr('href','javascript:;');
	});
	
 	$('.rec_list .btn').on('mouseout', function(){
		$('#qrcode').remove();
	});
}

function pc_qrcode_list(){
//	$('.game_list a').each(function(index){
//		$(this).attr('href','javascript:;');
//	});
	
	$('.down_btn').on('mouseover', function(){
		var obj = $(this);
		var app_url = obj.parent().attr('data-url');
		obj.append('<div id="qrcode"><div>手机浏览器扫码</div></div>');
		var qrcode = new QRCode(document.getElementById('qrcode'),{width:150,height:150});
		qrcode.makeCode(app_url); 
		//obj.parent().attr('href','javascript:;');
	});
	
	$('.down_btn').on('mouseout', function(){
		$('#qrcode').remove();
	});
	
	$('.more_qr').on('mouseover', function(){
		var obj = $(this);
		var app_url = obj.attr('data-url');
		if(app_url){
			obj.append('<div id="qrcode" class="qrcode_rec"><div>手机浏览器扫码</div></div>');
			$('#qrcode').css({top:'-2.05rem',left:0,right:0});
			var qrcode = new QRCode(document.getElementById('qrcode'),{width:150,height:150});
			qrcode.makeCode(app_url); 
			//obj.attr('href','javascript:;');
		}
	});
	
 	$('.more_qr').on('mouseout', function(){
		$('#qrcode').remove();
	}); 
}

function randomsort(a, b) {return Math.random()>.5 ? -1 : 1;}
function ua(s){var u=navigator.userAgent.toLowerCase();return u.indexOf(s)>-1;}
function get(n){var reg = new RegExp("(^|&)" + n + "=([^&]*)(&|$)", "i");var r = window.location.search.substr(1).match(reg);if (r != null) return unescape(r[2]); return '';}
function update_time(){
	var d = new Date(),h = d.getHours(),p=0,v=6;
	d = d.toLocaleDateString();
	d = d.replace(/\//g,'');
	for(var i=0;i<24/v;i++){if(h >= v * i &&  h < v * i + v){p = i}}
	return d + p;
}